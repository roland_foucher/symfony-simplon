<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExoController extends AbstractController
{
    #[Route('/exo-twig', name: 'app_exo')]
    public function index(): Response
    {
        $randNumber = rand(1, 100);
        $table = ['ga', 'bu', 'zo', 'mau'];
        return $this->render('exo.html.twig', [
            'random' => $randNumber,
            'table' => $table,
        ]);
    }
}
