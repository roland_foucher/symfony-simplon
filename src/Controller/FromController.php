<?php

namespace App\Controller;

use App\Entity\Person;

use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// use App\Entity\Person;

class FromController extends AbstractController


{
    private array $list = ['ga', 'bu', 'zo', 'meu'];
    private PersonRepository $personRepository;


    #[Route('/from', name: 'app_from')]
    public function index(Request $request): Response
    {
        $input = $request->get('name');

        if ($input) {
            $this->list[] = $input;
        }

        return $this->render('from/index.html.twig', [
            'view' => $input,
            'list' => $this->list,
        ]);
    }

    #[Route("/form-person", name: "form-person")]
    public function formPerson(Request $request): Response
    {
        $person = null;
        $this->personRepository = new PersonRepository();

        $name = $request->get('name');
        $age = $request->get('age');
        $personality = $request->get('personality');
        if ($name && $age && $personality) {
            $person = new Person();
            $person->setName($name);
            $person->setAge($age);
            $person->setPersinality($personality);
            $bool = $this->personRepository->save($person);
            dump($bool);
        }
        dump($person);

        $persons = $this->personRepository->findAll();
        dump($persons);
        $firstPerson = $this->personRepository->find(1);
        dump($firstPerson);



        return $this->render('from/form-person.html.twig', [
            'person' => $person,
        ]);
    }
}
