<?php

namespace App\Controller;

use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowPersonController extends AbstractController
{
    private PersonRepository $personRepository;

    #[Route('/', name: 'home')]
    public function index(): Response
    {
        $this->personRepository = new PersonRepository();
        $persons = $this->personRepository->findAll();

        return $this->render('show_person/index.html.twig', [
            'persons' => $persons,
        ]);
    }

    #[Route('/person/{id}', name: 'one_person')]
    public function onePerson(int $id): Response
    {
        $this->personRepository = new PersonRepository();
        $person = $this->personRepository->find($id);
        return $this->render('show_person/show-one.html.twig', [
            'person' => $person,
        ]);
    }

    #[Route('/delete/{id}', name: 'delete_person')]
    public function deletePerson(int $id): Response
    {
        $this->personRepository = new PersonRepository();
        $this->personRepository->deleteById($id);
        return $this->redirectToRoute('home');
    }
}
