<?php

namespace App\Entity;


class Person
{

    public function __construct(private int $age = 0, private string $name = '', private string $persinality = '', private ?int $id = null)
    {
        $this->age = $age;
        $this->name = $name;
        $this->id = $id;
        $this->persinality = $persinality;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getPersinality(): ?string
    {
        return $this->persinality;
    }

    public function setPersinality(string $persinality): self
    {
        $this->persinality = $persinality;

        return $this;
    }
}
