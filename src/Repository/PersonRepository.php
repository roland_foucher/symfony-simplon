<?php

namespace App\Repository;

use App\Entity\Person;
use PDO;
use PDOException;
use phpDocumentor\Reflection\Types\Boolean;

class PersonRepository
{
    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new PDO(
                'mysql:host=localhost;dbname=symfony',
                'simplon',
                '1234',
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
            );
        } catch (PDOException $e) {
            dump($e->getMessage());
        }
    }

    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM person');
        $query->execute();
        $result = $query->fetchAll();
        $list = [];
        foreach ($result as $line) {
            $person = $this->instanciatePerson($line);
            $list[] = $person;
        }
        return $list;
    }

    public function find(int $id): ?Person
    {
        $query = $this->pdo->prepare('SELECT * FROM person WHERE id=:id');
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();

        $line = $query->fetch();
        if ($line) {
            return $this->instanciatePerson($line);
        }
        return null;
    }

    public function save(Person $person): bool
    {
        $query = $this->pdo->prepare('INSERT INTO person (name, age, personality) VALUES (:name, :age, :personality)');
        $query->bindValue(':age', $person->getAge(), PDO::PARAM_INT);
        $query->bindValue(':name', $person->getName(), PDO::PARAM_STR);
        $query->bindValue(':personality', $person->getPersinality(), PDO::PARAM_STR);
        if ($query->execute()) {
            $person->setId(intval($this->pdo->lastInsertId()));
            return true;
        }

        return false;
    }

    public function update(Person $person): bool
    {
        $query = $this->pdo->prepare('UPDATE person SET name = :name, age = :age, personality = :personality WHERE id=:id');
        $query->bindValue(':age', $person->getAge(), PDO::PARAM_INT);
        $query->bindValue(':name', $person->getName(), PDO::PARAM_STR);
        $query->bindValue(':personality', $person->getPersinality(), PDO::PARAM_STR);
        $query->bindValue(':id', $person->getId(), PDO::PARAM_INT);

        return $query->execute();
    }

    public function deleteById(int $id): bool
    {
        $query = $this->pdo->prepare('DELETE FROM person WHERE id=:id');
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        return $query->execute();
    }

    private function instanciatePerson($line): Person
    {
        return new Person($line['age'], $line['name'], $line['personality'], $line['id']);
    }
}
